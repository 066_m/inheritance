/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.inheritance;

/**
 *
 * @author iUser
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "Brown", 0);
        animal.speak();
        animal.walk();
        
        Dog Dang = new Dog("Dang", "Black&White");
        Dang.speak();
        Dang.walk();
        
        Cat Zero = new Cat("Zero", "Orange");
        Zero.speak();
        Zero.walk();
        
        Duck Som = new Duck("Som", "Yellow");
        Som.speak();
        Som.walk();
        Som.fly();
        
        Dog To = new Dog("To", "Brown");
        To.speak();
        To.walk();
        
        Dog Mome = new Dog("Mome", "Black&White");
        Mome.speak();
        Mome.walk();
        
        Dog Bat = new Dog("Bat", "Black&White");
        Bat.speak();
        Bat.walk();
        
        Duck GabGab = new Duck("GabGab", "Black");
        GabGab.speak();
        GabGab.walk();
        GabGab.fly();
        
        
        System.out.println("|||||||");
        
        Animal[] animals = {Dang, Zero, Som, To, Mome, Bat, GabGab};
        for(int i = 0; i < animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            
            if(animals[i] instanceof Duck){
                //err; animals[i].fly();
                Duck duck = (Duck)animals[i]; //variable not new object
                duck.fly();
                //((Duck)animals[i]).fly();
            }
        }
        
        System.out.println("|||||||");
        
        System.out.println("Som is Animal : " + (Som instanceof Animal));
        System.out.println("Som is Duck : " + (Som instanceof Duck));
        //err; System.out.println("Som is Cat : " + (Som instanceof Cat));
        System.out.println("Som is Object : " + (Som instanceof Object));
        System.out.println("Animal is Dog : " + (animal instanceof Dog));
        System.out.println("Animal is Animal : " + (animal instanceof Animal));
        
        System.out.println("To is Animal : " + (To instanceof Animal));
        System.out.println("To is Dog : " + (To instanceof Dog));
        System.out.println("Dang is Animal : " + (Dang instanceof Animal));
        System.out.println("Dang is Dog : " + (Dang instanceof Dog));
        System.out.println("Zero is Animal : " + (Zero instanceof Animal));
        System.out.println("Zero is Cat : " + (Zero instanceof Cat));
        System.out.println("Mome is Animal : " + (Mome instanceof Animal));
        System.out.println("Mome is Dog : " + (Mome instanceof Dog));
        System.out.println("Bat is Animal : " + (Bat instanceof Animal));
        System.out.println("Bat is Dog : " + (Bat instanceof Dog));
        System.out.println("GabGab is Animal : " + (GabGab instanceof Animal));
        System.out.println("GabGab is Duck : " + (GabGab instanceof Duck));
        
        
        System.out.println("|||||||");
        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = Som;
        ani2 = Zero;
        Som.fly();
        //err; ani1.fly();
        
        System.out.println("Ani1 : Som is Duck : " +(ani1 instanceof Duck));
        
        
    }
}
